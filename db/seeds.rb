user1 = User.create! email: 'oguz@example.com', password: 'password', password_confirmation: 'password'
user2 = User.create! email: 'kaan@example.com', password: 'password', password_confirmation: 'password'

3.times do |i|
	user1.articles.create! title: Faker::Movies::StarWars.character, description: Faker::Movies::StarWars.quote
	user2.articles.create! title: Faker::Movies::StarWars.droid, description: Faker::Movies::StarWars.quote
end
