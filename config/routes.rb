Rails.application.routes.draw do
  get 'dashboard/index'
  devise_for :users

  resources :articles

  root 'dashboard#index'
end
